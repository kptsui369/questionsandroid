package hk.ust.cse.hunkim.questionroom;

import android.app.Activity;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.TouchUtils;
import android.test.UiThreadTest;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.EditText;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static java.util.regex.Pattern.matches;

/**
 * Created by Douglas_lkf on 10/31/2015.
 */
public class MainActivityUItest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Activity mActivity;
    private Button mSendButton;
    private EditText mInputBox;
    private String mRoom_name;
    private Button[] mButtonArray = new Button[3];
    private EditText mInputText;
    private Intent mIntent;

    public MainActivityUItest() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        // In setUp, you can create any shared test data,
        // or set up mock components to inject
        // into your Activity. But do not call startActivity()
        // until the actual test methods.
        // into your Activity. But do not call startActivity()
        // until the actual test methods.
        setActivityInitialTouchMode(true);
        mActivity = getActivity();
        mSendButton = (Button) mActivity.findViewById(R.id.sendButton);
        mInputBox = (EditText) mActivity.findViewById(R.id.messageInput);
        mButtonArray[0] = (Button) mActivity.findViewById(R.id.tag1);
        mButtonArray[1] = (Button) mActivity.findViewById(R.id.tag2);
        mButtonArray[2] = (Button) mActivity.findViewById(R.id.tag3);
        mInputText = (EditText) mActivity.findViewById(R.id.messageInput);

    }
/*
    @MediumTest
    public void testPreconditions() {
        //startActivity(mStartIntent, null, null);
        mButton = (Button) getActivity().findViewById(R.id.sendButton);
        assertNotNull(getActivity());
        assertNotNull(mButton);

        assertEquals("This is set correctly", "hk.ust.cse.hunkim.questionroom.MainActivity", getActivity().getTitle());
    }
    */
    @Test
    public void test_intent (){
        mIntent = mActivity.getIntent();
        Intent expected = null;
        assertNotSame("The intent should not be ", expected, mIntent);

        String expected_room_name = null;
        mRoom_name = mIntent.getStringExtra(JoinActivity.ROOM_NAME);
        assertEquals("The intent should be ", expected_room_name, mRoom_name);



    }

    @MediumTest
    public void testSendButton_layout() {

        final View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, mSendButton);

        final ViewGroup.LayoutParams mSendButtonLayoutParams = mSendButton.getLayoutParams();
        assertNotNull(mSendButtonLayoutParams);
        assertEquals("The width of the Send-Button should be", 210, mSendButtonLayoutParams.width);
        assertEquals("The height of the Send-Button should be", 120, mSendButtonLayoutParams.height);

    }

    @MediumTest
    public void testInputBox_layout (){

        final View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, mInputBox);

        final ViewGroup.LayoutParams mInputBoxLayoutParams = mSendButton.getLayoutParams();
        assertNotNull(mInputBoxLayoutParams);
        assertEquals("The width of the InputBox should be", 210, mInputBoxLayoutParams.width);
        assertEquals("The height of the InputBox should be", 120, mInputBoxLayoutParams.height);

    }

    @MediumTest
    public void testTagButton (){

        for (int j=0;j<=3;j++); {
            final View decorView = mActivity.getWindow().getDecorView();
            int i = 0;
            ViewAsserts.assertOnScreen(decorView, mButtonArray[i]);

            final ViewGroup.LayoutParams mButtonArrayLayoutParams = mButtonArray[i].getLayoutParams();
            assertNotNull(mButtonArrayLayoutParams);
            assertEquals("The width of the tag button should be", -2, mButtonArrayLayoutParams.width);
            assertEquals("The height of the tag button should be", 75, mButtonArrayLayoutParams.height);
            i++;
        }



    }

    @MediumTest
    public void test_the_behavior_of_the_send_button (){
        String expected = "COMP3111";


        onView(withId(R.id.messageInput)).perform(typeText(expected), closeSoftKeyboard());
        onView(withId(R.id.messageInput)).check(matches(withText(expected)));

        onView(withId(R.id.sendButton)).perform(click());

        expected = "#exam How to test the tag function?";
        onView(withId(R.id.messageInput)).perform(typeText(expected), closeSoftKeyboard());
        onView(withId(R.id.messageInput)).check(matches(withText(expected)));

        onView(withId(R.id.sendButton)).perform(click());

        expected = "";
        onView(withId(R.id.messageInput)).perform(typeText(expected), closeSoftKeyboard());
        onView(withId(R.id.messageInput)).check(matches(withText(expected)));

        onView(withId(R.id.sendButton)).perform(click());

        expected = "#testing How to get 50% coverage?";
        onView(withId(R.id.messageInput)).perform(typeText(expected), closeSoftKeyboard());
        onView(withId(R.id.messageInput)).check(matches(withText(expected)));

        onView(withId(R.id.sendButton)).perform(click());



        expected = "#COMP3111";
        onView(withId(R.id.messageInput)).perform(typeText(expected), closeSoftKeyboard());
        onView(withId(R.id.messageInput)).check(matches(withText(expected)));

        onView(withId(R.id.sendButton)).perform(click());


        /*
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                mInputText.requestFocus();
            }
        });
        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync(expected);
        getInstrumentation().waitForIdleSync();

        //TouchUtils.clickView(this, mSendButton);
        assertEquals("The text in the input box should be", expected, mInputText.getText());

        */


    }

    @Test
    public void testLocal()throws Exception {
        Class<?> test = Class.forName("hk.ust.cse.hunkim.questionroom.QuestionTest");
        JUnitCore junit = new JUnitCore();
        junit.run(test);

        /*
         test = Class.forName("hk.ust.cse.hunkim.questionroom.MainActivityTest");
        junit = new JUnitCore();
        junit.run(test);

        */
        test = Class.forName("hk.ust.cse.hunkim.questionroom.JoinActivityTest");
        junit = new JUnitCore();
        junit.run(test);

       /* test = Class.forName("hk.ust.cse.hunkim.questionroom.DBUtilTest");
        junit = new JUnitCore();
        junit.run(test);*/

        test = Class.forName("hk.ust.cse.hunkim.questionroom.CommentActivityTest");
        junit = new JUnitCore();
        junit.run(test);

        test = Class.forName("hk.ust.cse.hunkim.questionroom.ApplicationTest");
        junit = new JUnitCore();
        junit.run(test);


        test = Class.forName("hk.ust.cse.hunkim.questionroom.commentTest");
        junit = new JUnitCore();
        junit.run(test);

    }
}
