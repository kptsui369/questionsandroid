package hk.ust.cse.hunkim.questionroom;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hk.ust.cse.hunkim.questionroom.db.DBHelper;
import hk.ust.cse.hunkim.questionroom.db.DBUtil;
import hk.ust.cse.hunkim.questionroom.question.Question;

public class MainActivity extends ListActivity implements PopupMenu.OnMenuItemClickListener{

    //test git 2

    // TODO: change this to your own Firebase URL
    private static final String FIREBASE_URL = "https://intense-torch-3848.firebaseio.com/";

    //private final int[] tagColorTop = {Color.parseColor("#b7ff3535"), Color.parseColor("#dcf8a500"), Color.parseColor("#00c414")};
    public final static int tagColor = Color.parseColor("#31e2a1");

    private String roomName;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    private QuestionListAdapter mChatListAdapter;
    private EditText inputText;

    private DBUtil dbutil;

    public DBUtil getDbutil() {
        return dbutil;
    }

    private int previousPressed = 0;

    private ArrayList<String> tagsList;
    private String[] frequentTag;
    private Button[] tagsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialized once with an Android context.
        Firebase.setAndroidContext(this);

        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        assert (intent != null);

        // Make it a bit more reliable
        roomName = intent.getStringExtra(JoinActivity.ROOM_NAME);
        if (roomName == null || roomName.length() == 0) {
            roomName = "all";
        }

        TextView header = (TextView) findViewById(R.id.header);
        header.setText(roomName);

        tagsList = new ArrayList<>();
        frequentTag = new String[3];
        frequentTag[0] = "exam";
        frequentTag[1] = "assignment";
        frequentTag[2] = "lab";


        tagsButton = new Button[3];
        tagsButton[0] = (Button) findViewById(R.id.tag1);
        tagsButton[1] = (Button) findViewById(R.id.tag2);
        tagsButton[2] = (Button) findViewById(R.id.tag3);

        // Setup our Firebase mFirebaseRef
        mFirebaseRef = new Firebase(FIREBASE_URL).child(roomName).child("questions");

        // Setup our input methods. Enter key on the keyboard or pushing the send button
        inputText = (EditText) findViewById(R.id.messageInput);
        /*
        inputText.setRawInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });
        */

        findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = inputText.getText().toString();
                sendMessage(input);
            }
        });

        // get the DB Helper
        DBHelper mDbHelper = new DBHelper(this);
        dbutil = new DBUtil(mDbHelper);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        final ListView listView = getListView();
        // Tell our list adapter that we only want 200 messages at a time
        mChatListAdapter = new QuestionListAdapter(
                mFirebaseRef.orderByChild("timestamp").limitToLast(200),
                this,
                R.layout.question,
                roomName);
        listView.setAdapter(mChatListAdapter);
/*
        mChatListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(mChatListAdapter.getCount() - 1);
            }
        });
*/
        // Finally, a little indication of connection status

        // This method will be called anytime new data is added to our Firebase
        mConnectedListener = mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Disconnected", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });

        // Initialize tagsList and listen to tags
        mFirebaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildKey) {
                GenericTypeIndicator<List<String>> t = new GenericTypeIndicator<List<String>>() {
                };
                List<String> tags = dataSnapshot.child("tags").getValue(t);
                if(tagsList == null) tagsList = new ArrayList<>();
                if (tags != null) {
                    for (String tag : tags)
                        tagsList.add(tag);
                }
                setFrequentTag();
            }

            @Override
            public void onChildChanged(DataSnapshot snapshot, String previousChildKey) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        mFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mChatListAdapter.cleanup();
    }

    /*
    @Override
    protected void onListItemClick(ListView lv, View v, int position, long id) {
        super.onListItemClick(lv, v, position, id);

        TextView clickedView = (TextView) v;
        String topic = clickedView.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString("topic", topic);
        bundle.putInt("order", -1 - position); // (-1) - (0,1,2,3) because order start from -1,-2
        bundle.putString("roomName", roomName);

        Intent intent = new Intent();
        intent.setClass(MainActivity.this, CommentActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }
    */


    // Create a new Question Post
    private void sendMessage(String input) {
        if (!input.equals("")) {

            try{
                // Create our 'model', a Chat object
                Question question;
                // parse tags
                int tagStart = input.indexOf("#");
                if(tagStart > -1) {
                    int tagEnd = Question.findTagEndPos(input);
                    String onlyTags = input.substring(tagStart, tagEnd);
                    String[] tags = onlyTags.substring(1).replaceAll(" ", "").split("#"); // # at beginning => first element empty

                    question = new Question(input.replace(onlyTags, ""));
                    question.setTags(tags);
                }
                else
                    question = new Question(input);
                question.setOrder(-1 - mChatListAdapter.getCount()); // order start from -1, -2, -3
                question.setOp(JoinActivity.userName);

                // Extract Youtube Link
                String youtubeLink = Question.extractYoutubeLink(input);
                if(youtubeLink != "")
                    question.linkedDesc = youtubeLink + "\n";
                // Extract Image Link
                String imageLink = Question.extractImageLink(input);
                if(imageLink != "")
                    question.linkedDesc += "<img class=\"imgFrame\" src=\"" + imageLink + "\">\n";


                // Create a new, auto-generated child of that chat location, and save our chat data there
                Firebase postRef = mFirebaseRef.push();
                //String postId = postRef.getKey();
                postRef.setValue(question);

                inputText.setText("");
            }catch (Exception e){
                Log.e("Error", "sendMessage()");
            }

        }
    }

    // Increase number of like (echo)
    public void updateEcho(String key) {
        if (dbutil.contains(key)) {
            Log.e("Dupkey", "Key is already in the DB!");
            return;
        }

        final Firebase echoRef = mFirebaseRef.child(key).child("echo");
        echoRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long echoValue = (Long) dataSnapshot.getValue();
                        Log.e("Echo update:", "" + echoValue);

                        echoRef.setValue(echoValue + 1);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );
        /* disable: should do in sorting
        final Firebase orderRef = mFirebaseRef.child(key).child("order");
        orderRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long orderValue = (Long) dataSnapshot.getValue();
                        Log.e("Order update:", "" + orderValue);

                        orderRef.setValue(orderValue - 1);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );
        */

        // Update SQLite DB
        dbutil.put(key);
    }

    // Function to increase dislike (d_echo), call in QuestionListAdapter.java
    public void updateD_echo(String key) {
        if (dbutil.contains(key)) {
            Log.e("Dupkey", "Key is already in the DB!");
            return;
        }

        final Firebase echoRef = mFirebaseRef.child(key).child("d_echo");
        echoRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long d_echoValue = (Long) dataSnapshot.getValue();
                        Log.e("Echo update:", "" + d_echoValue);

                        echoRef.setValue(d_echoValue + 1);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );
        // Update SQLite DB
        dbutil.put(key);
    }

    public void Close(View view) {
        finish();
    }

    public void toCommentActivity(String key, String topic, String[] tags, String op,String time) {
        Intent intent = new Intent(this, CommentActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString("topic", topic);
        bundle.putString("key", key);
        bundle.putString("roomName", roomName);
        bundle.putStringArray("tags", tags);
        bundle.putString("op", op);
        bundle.putString("time", time);

        intent.putExtras(bundle);
        tagsList = null;
        startActivity(intent);
    }

    ////////////////////////////////
    // AlertDialog Functions
    ////////////////////////////////
    public void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.setting_menu);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeName:
                showSetNameDialog();
                return true;
            case R.id.about:
                Toast.makeText(this, "Hello, we are love!sung", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sortByTime:
                performSorting(Question.timeComparator);
                return true;
            case R.id.sortByComment:
                performSorting(Question.commentComparator);
                return true;
            case R.id.sortByPoster:
                performSorting(Question.opComparator);
                return true;
            case R.id.sortByLike:
                performSorting(Question.echoComparator);
                return true;
            case R.id.sortByDislike:
                performSorting(Question.d_echoComparator);
                return true;
            case R.id.sortByTopic:
                performSorting(Question.topicComparator);
                return true;
            default:
                return false;
        }
    }

    public void showSetNameDialog(){
        SetNameDialog dialog = new SetNameDialog();
        dialog.show(getFragmentManager(), "setNameDialog");
    }


    ////////////////////////////////
    // Sorting Functions
    ////////////////////////////////
    public void showSortingMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.sorting_menu);
        popup.show();
    }

    // Sorting on clicked
    public void performSorting(Comparator<Question> comparater){
        Question.sortingComparator = comparater;

        ListView listView = getListView();
        mChatListAdapter.cleanup();
        mChatListAdapter = new QuestionListAdapter(
                mFirebaseRef.orderByChild("timestamp").limitToLast(200),
                this,
                R.layout.question,
                roomName);
        listView.setAdapter(mChatListAdapter);
    }

    ////////////////////////////////
    // Tag Functions
    ////////////////////////////////
    public void tagOnClicked(View view){
        switch (view.getId()){
            case R.id.tag1:
                filterList(frequentTag[0], previousPressed, 1);
                break;
            case R.id.tag2:
                filterList(frequentTag[1], previousPressed, 2);
                break;
            case R.id.tag3:
                filterList(frequentTag[2], previousPressed, 3);
                break;
            default:
                break;
        }
    }

    public void filterList(String keyWord, int previousPressedTag, int triggerTag){
        //if(previousPressedTag == 0){}
        if(previousPressedTag != 0){
            resetList();
            ListView lv = getListView();
            lv.setAdapter(mChatListAdapter);
            this.previousPressed = 0;
            return;
        }
        //else // previous pressed = other tag
            //resetList();
        this.previousPressed = triggerTag;

        List<Question> qList = new ArrayList<>();

        for (int i=0; i < mChatListAdapter.getCount(); i++){
            // if tag contains substring exam. midterm, finl
            Question q = (Question) mChatListAdapter.getItem(i);
            String[] tags = q.getTags();
            String wholeMsg = q.getWholeMsg();
            if(tags == null) {
                continue;
            }

            if(wholeMsg.contains(keyWord))
                qList.add(q);
            else{
                for (int j = 0; j < tags.length; ++j) {
                    if (tags[j].contains(keyWord)) {
                        qList.add(q);
                        break;
                    }
                }
            }
        }
        mChatListAdapter.mModels.clear();
        mChatListAdapter.mModels = qList;
        ListView lv = getListView();
        lv.setAdapter(mChatListAdapter);

    }

    public void resetList(){
        mChatListAdapter.cleanup();
        mChatListAdapter = new QuestionListAdapter(
                mFirebaseRef.orderByChild("timestamp").limitToLast(200),
                this,
                R.layout.question,
                roomName);
    }

    public void setFrequentTag(){
        ArrayList<String> uniqueTags = new ArrayList<>();

        for(String tag : tagsList){
            if(!uniqueTags.contains(tag))
                uniqueTags.add(tag);
        }
        // find 3 frequent tags
        for(int i=0; i<3; i++){
            int max = 2;
            String frequentStr = null;
            for(String tag : uniqueTags){
                int occurrence = Collections.frequency(tagsList, tag);
                if(occurrence > max){
                    max = occurrence;
                    frequentStr = tag;
                }
            }
            if(frequentStr != null){
                frequentTag[i] = frequentStr;
                tagsButton[i].setText(frequentTag[i]);

                for(String tag : tagsList)
                    if(tag.equals(frequentStr))
                        uniqueTags.remove(tag);
            }
        }
    }

    // Image Upload
    public void toImageUploadActivity(View v) {
        Intent intent = new Intent(this, ImageUploadActivity.class);
        intent.putExtra("header", "New Question");
        startActivityForResult(intent, ImageUploadActivity.DATA_BACK_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUploadActivity.DATA_BACK_REQUEST && resultCode == RESULT_OK) {
            String msg = data.getStringExtra("msg");
            sendMessage(msg);
        }
    }

}
