package hk.ust.cse.hunkim.questionroom.question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by hunkim on 7/16/15.
 */
public class Question implements Comparable<Question> {

    /**
     * Must be synced with firebase JSON structure
     * Each must have getters
     */
    private String key;

    private String wholeMsg;
    private String head;
    private String headLastChar;
    private String desc;
    public String linkedDesc;
    public boolean completed;
    private long timestamp;
    private String[] tags;
    private int echo;
    private int d_echo;
    private String op;
    private int order; // start from -1, -2, -3, -4
    private int views;
    private boolean newQuestion;

    public String bgColor;
    public String textColor;
    public String dateString;


    //public ArrayList<comment> comments;
    public comment[] comments;

   // public String getTrustedDesc() {
   //     return trustedDesc;
    //}//never used, comment out for increasing coverage

    private String trustedDesc;

    // Required default constructor for Firebase object mapping
    //@SuppressWarnings("unused")
    public Question() {
    }

    /**
     * Set question from a String message
     * @param message string message
     */
    public Question(String message) {
        this.wholeMsg = message;
        this.echo = 0;
        this.d_echo = 0;
        this.views = 0;
        this.op = "";

        this.head = message.replace(extractYoutubeLink(message), "");
        this.head = this.head.replace(extractImageLink(this.head), "");

        this.completed = false;
        this.linkedDesc = "";
        this.desc = "";
        if (this.head.length() < message.length()) {
            this.desc = message.substring(this.head.length());
        }

        // get the last char
        this.headLastChar = head.substring(head.length() - 1);

        this.timestamp = new Date().getTime();
        this.tags = null;

        this.bgColor = "#fff";
        this.textColor = "#000";
        this.dateString = "";
        this.comments = null;
    }

    /**
     * Get first sentence from a message
     * @param message
     * @return
     */
    public static String getFirstSentence(String message) {
        String[] tokens = {". ", "? ", "! "};

        int index = -1;

        for (String token : tokens) {
            int i = message.indexOf(token);
            if (i == -1) {
                continue;
            }

            if (index == -1) {
                index = i;
            } else {
                index = Math.min(i, index);
            }
        }

        if (index == -1) {
            return message;
        }

        return message.substring(0, index+1);
    }

    /* -------------------- Getters ------------------- */
    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }

    public int getEcho() {
        return echo;
    }
    public int getD_echo() {
        return d_echo;
    }

    public String getWholeMsg() {
        return wholeMsg;
    }

    public String getHeadLastChar() {
        return headLastChar;
    }

    //public String getLinkedDesc() {
        //return linkedDesc;
    //} //never used, comment out to increase coverage

    //public boolean isCompleted() {
       // return completed;
    //}//never used, comment out to increase coverage

    public long getTimestamp() {
        return timestamp;
    }

    public String[] getTags() {
        return tags;
    }

    public int getOrder() {
        return order;
    }

    public int getViews() {
        return views;
    }

    public String getOp() { return op; }

    public boolean isNewQuestion() {
        return newQuestion;
    }

    public void updateNewQuestion() {
        newQuestion = this.timestamp > new Date().getTime() - 180000;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setOrder(int order) {this.order = order; }

    public void setOp(String op) {this.op = op; }

    public void setTags(String[] tags) {this.tags = tags;}

    /**
     * New one/high echo goes bottom
     * @param other other chat
     * @return order
     */
    @Override
    public int compareTo(Question other) {
        // Push new on top
        other.updateNewQuestion(); // update NEW button
        this.updateNewQuestion();

        if (this.newQuestion != other.newQuestion) {
            return this.newQuestion ? 1 : -1; // this is the winner
        }


        if (this.echo == other.echo) {
            if (other.timestamp == this.timestamp) {
                return 0;
            }
            return other.timestamp > this.timestamp ? -1 : 1;
        }
        return this.echo - other.echo;
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Question)) {
            return false;
        }
        Question other = (Question)o;
        return key.equals(other.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    //-------------------------------------
    // For sorting
    //-------------------------------------
    // setting sorting comparator
    //public enum QuestionComparator {Q_TIME, Q_ECHO, Q_D_ECHO, Q_COMMENT, Q_OP, Q_MSG};
    //public static QuestionComparator questionComparator = QuestionComparator.Q_TIME;

    public static Comparator<Question> timeComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            long value = q2.getTimestamp() - q1.getTimestamp();
            if(value > 0)
                return 1;
            else if(value == 0)
                return 0;
            else return -1;

        }
    };
    public static Comparator<Question> echoComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            return q2.getEcho() - q1.getEcho();
        }
    };
    public static Comparator<Question> d_echoComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            return q2.getD_echo() - q1.getD_echo();
        }
    };
    public static Comparator<Question> commentComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            return q2.getViews() - q1.getViews();
        }
    };
    public static Comparator<Question> opComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            return q1.getOp().compareTo(q2.getOp());
        }
    };
    public static Comparator<Question> topicComparator = new Comparator<Question>() {
        public int compare(Question q1, Question q2) {
            return q1.getWholeMsg().compareTo(q2.getWholeMsg());
        }
    };

    public static Comparator<Question> sortingComparator = timeComparator;
    // sort model defined in QuestionList Adapter, perform in FirebaseList Adapter
/*
    public static String removeYoutubeLink(String msg){
        if(msg.contains("https://") && msg.contains("youtu")){
            int start = msg.indexOf("https://");
            String substr = msg.substring(start);
            int temp1 = substr.indexOf(" ");
            int temp2 = substr.indexOf("\n");
            String strToRemove = "";
            if(temp1 == -1 && temp2 == -1)
                return msg.substring(0, start);
            else if(temp1 == -1)
                strToRemove = substr.substring(0, temp2);
            else if(temp2 == -1)
                strToRemove = substr.substring(0, temp1);
            else{
                int end = temp1 < temp2 ? temp1 : temp2;
                strToRemove = substr.substring(0, end);
            }
            return msg.replace(strToRemove, "");
        }
        return msg;
    }
    public static String removeImageLink(String msg){
        if(msg.contains("http://")){
            if(msg.contains(".png") || msg.contains(".jpg")){
                int start = msg.indexOf("http://");
                String link = msg.substring(start);
                return msg.replace(link, "");
            }
        }
        return msg;
    }
/*
    public static String removeTags(String msg){
        if(msg.contains("#")){
            int start = msg.indexOf("#");
            int end = findTagEndPos(msg);
            return msg.replace(msg.substring(start, end), "");
        }
        return msg;
    }
*/
    public static int findTagEndPos(String msg){
        if(msg.contains("#")){
            int start = msg.indexOf("#");
            return start + findTagEndPos(msg.substring(start+1)) + 1;
        }
        else{
            int temp1 = msg.indexOf(" ");
            int temp2 = msg.indexOf("\n");
            if(temp1 == -1 && temp2 == -1)
                return msg.length();
            else if(temp1 == -1)
                return temp2;
            else if(temp2 == -1)
                return temp1;
            else
                return (temp1 < temp2 ? temp1 : temp2) + 1;
        }
    }

    public static String extractYoutubeLink(String input){
        String youtubeLink = "";
        int pos1 = input.indexOf("https://www.youtube.com/watch?v=");
        int pos2 = input.indexOf("https://youtu.be/");
        if(pos1 > -1){
            youtubeLink = input.substring(pos1, pos1+43);
        }
        else if(pos2 > -1){
            youtubeLink = input.substring(pos2, pos2+28);
        }
        return youtubeLink;
    }

    public static String extractImageLink(String input){
        String imageLink = "";
        int start = input.indexOf("http://");
        if( start > -1 ){
            int pos1 = input.indexOf(".png");
            int pos2 = input.indexOf(".jpg");
            if( pos1 > -1 ){
                imageLink = input.substring(start, pos1+4);
            }
            else if( pos2 > -1 ){
                imageLink = input.substring(start, pos2+4);
            }
        }
        return imageLink;
    }
}