package hk.ust.cse.hunkim.questionroom;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import hk.ust.cse.hunkim.questionroom.question.Question;


/**
 * Created by hunkim on 7/15/15.
 */

public class QuestionTest  extends TestCase {
    Question q, q1;



    protected void setUp() throws Exception {
        super.setUp();

        q = new Question("Hello? This is very nice");
        Thread.sleep(1000);
        q1 = new Question("Hello? This is very nice and message 2");

    }

    @SmallTest
    public void testChatFirstString() {
        String[] strHead = {
                "Hello? This is very nice", "Hello?",
                "This is cool! Really?", "This is cool!",
                "How.about.this? Cool", "How.about.this?"
        };

        for (int i=0; i<strHead.length; i+=2) {
            String head = q.getFirstSentence(strHead[i]);
            assertEquals("Chat.getFirstSentence", strHead[i+1], head);
        }
    }

    @SmallTest
    public void testHead() {
        assertEquals("Head", "Hello? This is very nice", q.getHead());
    }

    @SmallTest
    public void testDesc(){
        assertEquals("desc","", q.getDesc());
    }

    @SmallTest
    public void testWholeMsg(){
        assertEquals("wholeMsg","Hello? This is very nice", q.getWholeMsg());
    }

    @SmallTest
    public  void testGetHeadLastChar(){
        assertEquals("getHeadLastChar","e",q.getHeadLastChar());
    }

    @SmallTest
    public  void testGetViews(){
        assertEquals("getViews",0, q.getViews());
    }

    @SmallTest
    public void testSetOp(){
        q.setOp("Janek ist der Beste");
        assertEquals("setOp","Janek ist der Beste", q.getOp());
    }

    @SmallTest
    public void testGetOp(){
        q.setOp("..und schönste");
        assertEquals("setOp", "..und schönste", q.getOp());
    }

    @SmallTest
    public void testSetOrder(){
        q.setOrder(-4);
        assertEquals("setOrder",-4,q.getOrder());
    }

    @SmallTest
    public void testGetOrder(){
        q.setOrder(4);
        assertEquals("getOrder", 4, q.getOrder());
    }

    @SmallTest
    public void testSetKey(){
        q.setKey("wertzujiz");
        assertEquals("setKey","wertzujiz", q.getKey());
    }

    @SmallTest
    public void testGetKey(){
        q.setKey("werdfghjhk");
        assertEquals("getKey", "werdfghjhk", q.getKey());
    }

    @SmallTest
    public void testNewQuestion(){
        Question q2 = new Question("This is a new question");

        //compareTo test
        assertFalse("isNewQuestion", q2.isNewQuestion());
    }

    /*
    @SmallTest
    public void testEquals(){
        assertFalse("instanceof", e1.equals(s1));
        assertFalse("Not same object", e1.equals(e2));

    }
    */

    @SmallTest
    public void testTimeComparator(){
        assertEquals("timestamp", 1, Question.timeComparator.compare(q,q1));
        assertEquals("timestamp", -1, Question.timeComparator.compare(q1,q));
        assertEquals("timestamp", 0, Question.timeComparator.compare(q,q));
    }

    @SmallTest
    public void testD_echoComparator(){
        assertEquals("d_echo", 0, Question.d_echoComparator.compare(q, q1));
    }

    @SmallTest
    public void testCommentComparator(){
        assertEquals("comment", 0, Question.commentComparator.compare(q, q1));
    }

    @SmallTest
    public void testEchoComparator(){
        assertEquals("echo", 0, Question.echoComparator.compare(q, q1));

    }

    @SmallTest
    public void testOpComparator(){
        assertEquals("op", 0, Question.opComparator.compare(q, q1));
    }

    @SmallTest
    public void testSortingComparator(){
        assertEquals("sorting", 1, Question.sortingComparator.compare(q, q1));
    }

    @SmallTest
    public void testTopicComparator(){
        assertEquals("topic", -14, Question.topicComparator.compare(q, q1));
    }

    @SmallTest
    public void testFindTagEndPos(){
        String msg1 = "Message with hashtag #hier";
        String msg2 = " ";
        String msg3 = "\n";
        String msg4 = " \n djendej";

        assertEquals("findTagEndPos", 26, q.findTagEndPos(msg1));
        assertEquals("findTagEndPos", 0, q.findTagEndPos(msg2));
        assertEquals("findTagEndPos", 0, q.findTagEndPos(msg3));
        assertEquals("findTagEndPos", 1, q.findTagEndPos(msg4));
    }

    @SmallTest
    public void testRemoveImageLink(){
        String msg = "http://fick-deine-mutter.png";
        assertEquals("fick-deine-mutter", "", msg.replace(q.extractYoutubeLink(msg), ""));
    }

    @SmallTest
    public void testRemoveYoutubeLink(){
        String msg1 = "https://youtu";
        String msg2 = "https:// youtu\\n";
        String msg3 = "https://youtu\\n";
        String msg4 = "https:// youtu";

        assertEquals("RemoveYoutubeLink", "", msg1.replace(q.extractYoutubeLink(msg1), ""));
        assertEquals("RemoveYoutubeLink", " youtu\\n", msg2.replace(q.extractYoutubeLink(msg2), ""));
        assertEquals("RemoveYoutubeLink", "", msg3.replace(q.extractYoutubeLink(msg3), ""));
        assertEquals("RemoveYoutubeLink", " youtu", msg4.replace(q.extractYoutubeLink(msg4), ""));
    }

    @SmallTest
    public void testHashcode(){
        Question pq = new Question("blaMaster");
        String key = "wasch willst du tun?";
        pq.setKey(key);
        assertEquals("hashcode", key.hashCode(), pq.hashCode());
    }

    @SmallTest
    public void testEquals(){
        Question pq = new Question("blaMaster");
        String key = "wasch willst du tun?";
        pq.setKey(key);
        assertEquals("hashcode", true, pq.equals(pq));
    }

    @SmallTest
    public void testCompareTo(){
        Question q1,q2;
        q1 = new Question("spast1");
        q2 = new Question("spast2");

        assertEquals("compareTo", 0, q1.compareTo(q2));
        assertEquals("compareTo", 0, q2.compareTo(q2));

    }

    @SmallTest
    public void testSetTags(){
        Question pq = new Question("blaMaster");
        String[] s = {"Du","bist","ein","spast"};
        pq.setTags(s);

        assertEquals("setTags", s, pq.getTags());
    }

    @SmallTest
    public void testUpdateNewQuestion(){
        Question pq = new Question("blaMaster");
        pq.updateNewQuestion();
        assertEquals("updateNewQuestion", true, true);
    }
}
